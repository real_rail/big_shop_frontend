import {Product} from "../models/product";
import {Http, Headers, RequestOptions, URLSearchParams} from "@angular/http";
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProductService {
    // private apiUrl = 'api/products';
    private apiUrl = 'http://localhost:8080/products';

    products: Product[] = [];
    product: Product;

    constructor(private http: Http) {}

    getProducts(): Promise<Product[]> {
        return this.http.get(this.apiUrl)
            .toPromise()
            .then(res => res.json())
            .then(products => this.products = products)
            .catch(this.handleError)
    }

    getProduct(id: number): Promise<Product> {
        let params: URLSearchParams = new URLSearchParams();
        params.append('id', id.toString());

        return this.http.get(this.apiUrl + "/" + id.toString(), {search: params})
            .toPromise()
            .then(res => res.json())
            .then(product => this.product = product)
            .catch(this.handleError);
    }

    createProduct(title: string) {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions(headers);
        let product = new Product(title);
        this.http.post(this.apiUrl, product, options)
            .toPromise()
            .then(res => res.json())
            .then(product => this.products.push(product))
            .catch(this.handleError)
    }

    private handleError(error: any) {
        console.error('Произошла ошибка', error);
        return Promise.reject(error.message || error)
    }
}