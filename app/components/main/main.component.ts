import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'main.component.html',
    styleUrls: ['main.component.css']
})
export class MainComponent {}