export class Product {
    id: number;
    constructor(public title: string,
                public description: string = null,
                public image_url: string = null,
                public price: string = null) {}
}