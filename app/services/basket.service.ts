import {Injectable} from "@angular/core";
import {Http, URLSearchParams} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import {BasketElement} from "../models/basket";
import {UserService} from "./user.service";
import {Product} from "../models/product";

@Injectable()
export class BasketService {
    private url = 'http://localhost:81/basket.php';

    basketElements: BasketElement[] = [];
    basketElement: BasketElement;

    constructor(private http: Http, private userService: UserService) {}

    getBasketElements(): Promise<BasketElement[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.append('token', this.userService.getToken());

        return this.http.get(this.url, {search: params})
            .toPromise()
            .then(res => res.json().data)
            .then(basketElements => this.basketElements = basketElements)
            .catch(this.handleError)
    }

    getProduct(id: number): Promise<BasketElement> {
        let params: URLSearchParams = new URLSearchParams();
        params.append('id', id.toString());

        return this.http.get(this.url, {search: params})
            .toPromise()
            .then(res => res.json().data)
            .then(product => this.basketElement = product)
            .catch(this.handleError);
    }

    addToCart(productId: number) {
        let params: URLSearchParams = new URLSearchParams();
        params.append('method', 'add');
        params.append('product_id', productId.toString());
        params.append('token', this.userService.getToken());

        return this.http.get(this.url, {search: params})
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('Произошла ошибка', error);
        return Promise.reject(error.message || error)
    }

    private removeFromBasketElements(basketElement: BasketElement) {
        const index: number = this.basketElements.indexOf(basketElement);
        if (index !== -1) {
            this.basketElements.splice(index, 1);
        }
    }

    removeFromCart(basketValue: BasketElement) {
        let params: URLSearchParams = new URLSearchParams();
        params.append('method', 'remove');
        params.append('product_id', basketValue.id_product.toString());
        params.append('token', this.userService.getToken());

        return this.http.get(this.url, {search: params})
            .toPromise()
            .then(res => res.json().data)
            .then(basketItem => this.removeFromBasketElements(basketValue))
            .catch(this.handleError);
    }
}