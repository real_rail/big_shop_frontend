import {Http, URLSearchParams} from "@angular/http";
import {EventEmitter, Injectable} from "@angular/core";
import 'rxjs/add/operator/toPromise';
import {User} from "../models/user";

@Injectable()
export class UserService {
    tokenUpdated = new EventEmitter();
    token: string;

    private authUrl = 'http://localhost:81/auth.php';
    private regUrl = 'http://localhost:81/reg.php';
    private userUrl = 'http://localhost:81/user.php';

    user: User;

    constructor(private http: Http) {}

    authentication(login: string, password: string) {
        let params: URLSearchParams = new URLSearchParams();
        params.append('login', login);
        params.append('password', password);

        let token: string = null;

        return this.http.get(this.authUrl, {search: params})
            .toPromise()
            .then(res => token = res.json().data.token.toString())
            .catch(this.handleError);
    }

    registration(user: User) {
        let params: URLSearchParams = new URLSearchParams();
        params.append('login', user.login);
        params.append('password', user.password);
        params.append('name', user.name);

        let token: string = null;

        return this.http.get(this.regUrl, {search: params})
            .toPromise()
            .then(res => token = res.json().token.toString())
            .catch(this.handleError);
    }

    getUserData(token: string) {
        let params: URLSearchParams = new URLSearchParams();
        params.append('token', token);

        let name: string = null;

        return this.http.get(this.userUrl, {search: params})
            .toPromise()
            .then(res => res.json().data)
            .then(user => this.user = user)
            .catch(this.handleError)
    }

    private handleError(error: any) {
        console.error('Произошла ошибка', error);
        return Promise.reject(error.message || error)
    }

    public setToken(token: string)
    {
        localStorage.setItem('token',  token);
        this.token = token;
        this.tokenUpdated.emit(this.token);
    }

    public getToken()
    {
        return localStorage.getItem('token');
    }

    public isTokenValid(token: string): boolean
    {
        return (token != null && token != '0' && token != 'null');
    }
}