import {Component} from "@angular/core";
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";
import {User} from "../../../models/user";

@Component({
    moduleId: module.id,
    selector: 'reg-form',
    templateUrl: 'reg-form.component.html',
    styleUrls: ['reg-form.component.css']
})
export class RegFormComponent {
    login: string = ' ';
    password: string = '';
    password_repeat: string = '';
    name: string = '';

    constructor(private userService: UserService, private router: Router) {}

    onSubmit() {
        if (this.password_repeat == this.password){
            let user = new User(null, this.login, this.name, null, this.password);
            this.userService.registration(user).then(token => this.afterAuth(token));
        }
    }

    afterAuth(token: string) {
        this.userService.setToken(token);
        if (this.userService.isTokenValid(this.userService.getToken()))
            this.router.navigate(['/catalog']);
        else alert('Неправильные данные');
    }
}