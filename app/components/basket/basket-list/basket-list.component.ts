import {Component, OnInit} from "@angular/core";
import {BasketElement} from "../../../models/basket";
import {BasketService} from "../../../services/basket.service";

@Component({
    moduleId: module.id,
    selector: 'basket-list',
    templateUrl: 'basket-list.component.html',
    styleUrls: ['basket-list.component.css']
})
export class BasketListComponent implements OnInit {
    basketElements: BasketElement[];

    constructor(private basketService: BasketService) {
        this.basketElements = [];
    }

    ngOnInit() {
        this.basketService.getBasketElements().then(basketElements => this.basketElements = basketElements);
    }
}