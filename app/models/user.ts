export class User {
    constructor(public id: number = null,
                public login: string = null,
                public name: string = null,
                public token: string = null,
                public password: string = null) {}
}