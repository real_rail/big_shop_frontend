import {Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../../models/user";
import {UserService} from "../../services/user.service";

@Component({
    moduleId: module.id,
    selector: 'header-part',
    templateUrl: 'header-part.component.html',
    styleUrls: ['header-part.component.css']
})
export class HeaderPartComponent {
    title: string = 'Big Shop';
    currentToken: string;
    @Input() currentName: string;
    @Input() currentUser: User;

    onLogOut() {
        this.currentToken = null;
        this.userService.setToken(null);
    }

    constructor (private userService: UserService) {
        this.currentToken = this.userService.getToken();
    }

    ngOnInit(){
        if (this.currentToken != '0' && this.currentToken != null) {
            this.userService.getUserData(this.currentToken).then(user => this.setUserData(user));
        }

        this.userService.tokenUpdated.subscribe(
            (token: string) => {
                this.userService.getUserData(token).then(user => this.setUserData(user))
            }
        );
    }

    setUserData(user: User){
        this.currentUser = user;
        this.currentToken = this.currentUser.token;
    }
}